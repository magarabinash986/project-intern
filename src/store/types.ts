import { IProject } from '@/models/project';
import { ILeave } from '@/models/leave';
import { IUser } from '@/models/user';
import { INotice } from '@/models/notice';
import { EmpProjectlist } from '@/models/employeeproject';

export interface RootState {
  modules: {
      project: IProject[] | null;
      leave: ILeave[] | null;
      user: IUser[] | null;
      notice: INotice[] | null;
      userdetails: IUser | null;
      empprojects: EmpProjectlist[] |null;
  };
}
