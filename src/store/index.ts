import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import { Project } from './modules/project';
import { Leave } from './modules/leave';
import { User} from '@/store/modules/listemployee';
import { Notice } from './modules/notice';
import { Personal } from './modules/personaldetails';
import {EmpProject} from './modules/empproject';
import {Auth} from './modules/auth';
import { RootState } from './types';

Vue.use(Vuex);

const store: StoreOptions<RootState> = {
  modules: {
    Project,
    Leave,
    User,
    Notice,
    Personal,
    EmpProject,
    Auth,
  },
};

export default new Vuex.Store<RootState>(store);
