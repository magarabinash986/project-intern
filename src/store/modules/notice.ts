import { ActionTree, Module, MutationTree, GetterTree } from 'vuex';
import noticeservices from '../../services/noticeServices/noticeservices';
import { INotice } from '@/models/notice';
import { RootState } from '../types';

export interface ModuleState {
    notice: INotice[] | null;
}

const state: ModuleState = {
    notice: null,
};

export const mutations: MutationTree<ModuleState> = {
    SET_NOTICE(thisstate, data): void {
        thisstate.notice = data;
    },
};

export const getters: GetterTree<ModuleState, RootState> = {
    GET_NOTICE(thisstate) {
        return thisstate.notice;
    },
};

export const actions: ActionTree<ModuleState, RootState> = {
    GET_NOTICE: async ({commit}) => {
        try {
            const res = await noticeservices.GET_NOTICES();
            commit('SET_NOTICE', res);
        } catch (error) {
            console.log(error.message);
        }
    },
};

export const Notice: Module<ModuleState, RootState> = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};
