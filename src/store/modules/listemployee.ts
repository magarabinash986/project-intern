import {Module, ActionTree, MutationTree, GetterTree} from 'vuex';
import UserServices from '@/services/userservices';
import {IUser} from '@/models/user';
import {RootState} from '@/store/types';

export interface ModuleState {
    user: IUser[] | null;
    count: number | null;

}

const state: ModuleState = {
    user: null,
    count: null,

};

export const mutations: MutationTree<ModuleState> = {
    SET_USER(thisstate, data): void {
        thisstate.user = data;
    },
    SET_COUNT(thisstate) {
        let count = 0;
        thisstate.user?.map((user) => {
            if (user.role === 'User') {
                count ++;
            }
        });
        thisstate.count = count;
    },
};

export const getters: GetterTree<ModuleState, RootState> = {
    GET_USER(thisstate) {
        const user = thisstate.user?.filter((item) => item.role === 'User');
        return user;
    },
    GET_TOTAL_USER(thisstate) {
        return thisstate.count;
    },

};

export const actions: ActionTree<ModuleState, RootState> = {
    GET_USER: async ({commit}) => {
        try {
            const user = await UserServices.getUser();
            commit('SET_USER', user);
            commit('SET_COUNT');
        } catch (error) {
            console.log(error.message);
        }
    },
};

export const User: Module<ModuleState, RootState> = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};
