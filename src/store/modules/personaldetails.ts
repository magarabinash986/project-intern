import { Module, ActionTree, GetterTree, MutationTree} from 'vuex';
import { IUser } from '../../models/user';
import { RootState } from '../types';
import personaldetailservices from '../../services/personaldetailservices';

export interface ModuleState {
    user: IUser | null;
}

const state: ModuleState = {
    user: null,
};

export const mutations: MutationTree<ModuleState> = {
    SET_PERSONAL(thisstate, data) {
        thisstate.user = data;
    },
};

export const actions: ActionTree<ModuleState, RootState> = {
    GET_PERSONAL: async ({commit}, id: string) => {
        try {
            const res = await personaldetailservices.GET_DETAILS(id);
            commit('SET_PERSONAL', res);
        } catch (error) {
            console.log(error.message);
        }
    },
};

export const getters: GetterTree<ModuleState, RootState> = {
    GET_PERSONAL(thisstate) {
        return thisstate.user;
    },
};

export const Personal: Module<ModuleState, RootState> = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};
