import { ActionTree, Module, MutationTree, GetterTree } from 'vuex';
import EmployeeprojectServices from '@/services/employeeProjectServices/employeeproject';
import { EmpProjectlist } from '@/models/employeeproject';
import { RootState } from '../types';

export interface ModuleState {
    empproject: EmpProjectlist[] | null;
}

const state: ModuleState = {
    empproject: null,
};

export const mutations: MutationTree<ModuleState> = {
    SET_EMPPROJECTS(thisstate, data): void {
        thisstate.empproject = data;
    },
};

export const getters: GetterTree<ModuleState, RootState> = {
    GET_EMPPROJECTS(thisstate) {
        return thisstate.empproject;
    },
};

export const actions: ActionTree<ModuleState, RootState> = {
    GET_EMPPROJECTS: async ({commit}) => {
        try {
            const empproject = await EmployeeprojectServices.getProject();
            commit('SET_EMPPROJECTS', empproject);
        } catch (error) {
            console.log(error.message);
        }
    },
};

export const EmpProject: Module<ModuleState, RootState> = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};

