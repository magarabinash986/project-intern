import { ActionTree, Module, MutationTree, GetterTree } from 'vuex';
import ProjectServices from '@/services/projectServices/projectservices';
import { IProject } from '@/models/project';
import { RootState } from '../types';

export interface ModuleState {
    project: IProject[] | null;
    count: number | null;
}

const state: ModuleState = {
    project: null,
    count: null,
};

export const mutations: MutationTree<ModuleState> = {
    SET_PROJECT(thisstate, data): void {
        thisstate.project = data;
    },
    SET_COUNT(thisstate) {
        let count = 0;
        thisstate.project?.map((item) => {
            count ++;
        });
        thisstate.count = count;
    },
};

export const getters: GetterTree<ModuleState, RootState> = {
    GET_PROJECTS(thisstate) {
        return thisstate.project;
    },
    GET_TOTAL_PROJECTS(thisstate) {
        return thisstate.count;
    },
};

export const actions: ActionTree<ModuleState, RootState> = {
    GET_PROJECTS: async ({commit}) => {
        try {
            const project = await ProjectServices.getProject();
            commit('SET_PROJECT', project);
            commit('SET_COUNT');
        } catch (error) {
            console.log(error.message);
        }
    },
};

export const Project: Module<ModuleState, RootState> = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};
