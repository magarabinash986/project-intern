import { ActionTree, GetterTree, MutationTree, Module } from 'vuex';
import { RootState } from '../types';
import AuthServices from '@/services/authServices/authservices';
import { IUser } from '@/models/user';

export interface ModuleState {
    loggedIn: boolean;
    user: IUser[] | null;
}

const state: ModuleState = {
    loggedIn: false,
    user: null,
};

export const mutations: MutationTree<ModuleState> = {
    SET_LOGGEDIN(thisstate, data): void {
        thisstate.user = data;
        thisstate.loggedIn = true;
    },
};


export const getters: GetterTree<ModuleState, RootState> = {
    GET_LOGGEDIN(thisstate) {
        return thisstate.loggedIn;
    },
    GET_USER(thisstate) {
        return thisstate.user;
    },
};

export const actions: ActionTree<ModuleState, RootState> = {
    GET_LOGGEDIN: async ({commit}, data) => {
        try {
            const auth = await AuthServices.logIn(data);
            if (auth.length !== 0) {
                commit('SET_LOGGEDIN', auth);
            } else {
                throw new Error('Credential didn\'t matched');
            }
        } catch (error) {
            console.log(error.message);
        }
    },
};

export const Auth: Module<ModuleState, RootState> = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
