import { ActionTree, Module, MutationTree, GetterTree } from 'vuex';
import LeaveServices from '@/services/leaveServices/leaveservices';
import { RootState } from '../types';
import { ILeave } from '@/models/leave';

export interface ModuleState {
    leave: ILeave[] | null;
    count: number | null;
}

const state: ModuleState = {
    leave: null,
    count: null,
};

export const mutations: MutationTree<ModuleState> = {
    SET_LEAVES(thisstate, data): void {
        thisstate.leave = data;
    },
    REPLACE(thisstate, data): void {
        thisstate.leave?.map((item: any) => {
            if (item.id === data.id) {
                item.id = data.id;
                item.date_of_leave = data.date_of_leave;
                item.reason = data.reason;
                item.approval = data.approval;
                item.employee = data.employee;
            }
        });
    },
    SET_COUNT(thisstate) {
        let count = 0;
        thisstate.leave?.map((item) => {
            count ++;
        });
        thisstate.count = count;
    },
};

export const getters: GetterTree<ModuleState, RootState> = {
    GET_LEAVES(thisstate) {
        return thisstate.leave;
    },
    GET_LEAVE_COUNT(thisstate) {
        return thisstate.count;
    },
};

export const actions: ActionTree<ModuleState, RootState> = {

    GET_LEAVES: async ({commit}) => {
        try {
            const project = await LeaveServices.GET_LEAVE();
            commit('SET_LEAVES', project);
            commit('SET_COUNT');
        } catch (error) {
            console.log(error.message);
        }
    },

    SET_REJECT: async ({commit}, data: ILeave) => {
        try {
            const approve = await LeaveServices.SET_APPROVAAL(data);
            commit('REPLACE', approve);
        } catch (error) {
            console.log(error.message);
        }
    },

    SET_ACCEPT: async ({commit}, data: ILeave) => {
        try {
            const approve = await LeaveServices.SET_APPROVAAL(data);
            commit('REPLACE', approve);
        } catch (error) {
            console.log(error.message);
        }
    },
};

export const Leave: Module<ModuleState, RootState> = {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};
