import {ListUser} from '@/models/user';
import axios from 'axios';

class UserServices {
    public async getUser(): Promise<ListUser> {

        const result = await axios.get('http://localhost:3000/user');
        return result.data;

    }
}

export default new UserServices();
