import axios from 'axios';
import { ProjectList } from '@/models/project';

const baseUrl = 'http://localhost:3000/Project';

class Projectservices {
    public async getProject(): Promise<ProjectList> {
        const result = await axios.get(baseUrl);
        return result.data;
    }
}

export default new Projectservices();
