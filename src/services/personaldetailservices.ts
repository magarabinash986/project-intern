import { IUser } from '@/models/user';
import axios from 'axios';

const baseUrl = 'http://localhost:3000/User/';

class PersonalDetails {

    public async GET_DETAILS(id: string): Promise<IUser> {
        const result = await axios.get(baseUrl + id);
        return result.data;
    }

}

export default new PersonalDetails();
