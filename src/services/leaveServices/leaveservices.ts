import { ILeave } from '@/models/leave';
import axios from 'axios';

const baseUrl = 'http://localhost:3000/leave';

interface LeaveList {
    leave?: ILeave[];
}

class Leaveservices {

    public async GET_LEAVE(): Promise<LeaveList> {
        const result = await axios.get(baseUrl);
        return result.data;
    }

    public async SET_APPROVAAL(data: ILeave): Promise<ILeave> {
        const result = await axios({
            method: 'put',
            url: baseUrl + '/' + data.id,
            data: {
                id: data.id,
                reason: data.reason,
                date_of_leave: data.date_of_leave,
                employee: data.employee,
                approval: data.approval,
            },
          });
        return result.data;
    }

}

export default new Leaveservices();
