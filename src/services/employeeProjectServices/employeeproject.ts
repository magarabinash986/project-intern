import axios from 'axios';
import { Empproject } from '@/models/employeeproject';

const baseUrl = 'http://localhost:3000/Project';

class EmployeeprojectServices {
    public async getProject(): Promise<Empproject> {
        const result = await axios.get(baseUrl);
        return result.data;
    }
}

export default new EmployeeprojectServices();
