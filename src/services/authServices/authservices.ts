import { IUser } from '@/models/user';
import axios from 'axios';

const baseUrl = 'http://localhost:3000/User';

class AuthServices {
    public async logIn(data: {username: string, password: string}): Promise<IUser[]> {
        const users = await axios.get(baseUrl);
        const res = users.data.filter((item: IUser) => {
            if (item.username === data.username && item.password === data.password) {
                return item;
            } else {
                return null;
            }
        });
        return res;
    }
}

export default new AuthServices();
