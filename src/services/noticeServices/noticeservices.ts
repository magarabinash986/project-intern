import axios from 'axios';
import { NoticeList } from '@/models/notice';

const baseUrl = 'http://localhost:3000/Notice';

class NoticeServices {
    public async GET_NOTICES(): Promise<NoticeList> {
        const result = await axios.get(baseUrl);
        return result.data;
    }
}

export default new NoticeServices();
