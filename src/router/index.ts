import Vue from 'vue';
import VueRouter, {RouteConfig} from 'vue-router';
import Home from '@/views/Home.vue';
import About from '@/views/About.vue';
import Admin from '@/views/admin/Index.vue';
import Employee from '@/views/employee/Index.vue';
import EmployeeList from '@/components/admin/listEmployee/EmployeeList.vue';
import ProjectList from '@/components/admin/listProject/ProjectList.vue';
import Leavelist from '@/components/admin/listLeave/LeaveList.vue';
import ProjectDetails from '../components/admin/projectDetails/ProjectDetails.vue';

Vue.use(VueRouter);

const routes: RouteConfig[] = [
    {
        path: '/',
        name: 'Home',
        component: Home,
    },
    {
        path: '/about',
        name: 'About',
        component: About,
    },
    {
        path: '/admin',
        name: 'Admin',
        component: Admin,
        children: [
            {path: '/admin/projects', component: ProjectList},
            {path: '/admin/leave', component: Leavelist},
            {path: '/admin/employee', component: EmployeeList},
        ],
    },
    {
        path: '/employee',
        name: 'Employee',
        component: Employee,

    },
    {
        path: '/listEmployee',
        name: 'EmployeeList',
        component: EmployeeList,
    },
    {
      path: '/project-details/:id',
      name: 'ProjectDetails',
      component: ProjectDetails,
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

export default router;
