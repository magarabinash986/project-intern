export interface IUser {
    id: string;
    username: string;
    role: string;
    password: string;
    contact: string;
    email: string;
    address: string;
    department: string;
}

export interface ListUser {
    user?: IUser[];
}
