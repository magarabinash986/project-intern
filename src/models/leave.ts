export interface ILeave {
    id: string;
    reason: string;
    date_of_leave: string;
    employee: string;
    approval: boolean;
}
