export interface INotice {
    id: string;
    desc: string;
    date: string;
    open: boolean;
}

export interface NoticeList {
    notice?: INotice[];
}
