export interface IProject {
    id: string;
    name: string;
    client: string;
    start: string;
    end: string;
}

export interface ProjectList {
    projects?: IProject[];
}
