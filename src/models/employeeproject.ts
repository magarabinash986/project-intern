export interface EmpProjectlist {
    id: string;
    name: string;
    client: string;
    start: string;
    end: string;
  }


export interface Empproject {
    empprojects?: EmpProjectlist[];
}

