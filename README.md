# hrm

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Task we need to do for this software
Data Needed:
	Employee Information
	Project Information
	Attendance Information
	Leave Information
	Notice Information
	Department

Table for the system:
	User: role based
		admin
		employee
	Project
	Attendance
	Leave
	Notice
	Department


Relation between Data:
	employee - project: many to many
	employee - attendance: one to many
	employee - leave: one to many
	admin - notice: one to many
	employee - department: many to one


Admin:
	view employees data
	view project available
	view employee working on the specific project
	respond to the leave
	create notices
	add or remove employees and project data
	assign project to the employee
	

Employee:
	View their data
	view notices
	edit their personal data
	leave module // Display will be done for later
	attendance module // Display will be done for later


Naming Guide:
	Folder name: first letters must be small rest be camelcase
	Vue file: CamelCase
	TS file: all in small letters

